# Install development requirements

tox
coverage
genbadge
flake8

-r requirements.txt
